from keras.models import load_model
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split #to split out training and testing data 
from utils import test_set_image_prep

data_df = pd.read_csv('data/driving_log.csv', names=['center', 'left', 'right', 'steering', 'throttle', 'reverse', 'speed'])
x = data_df[['center', 'left', 'right']].values
y = data_df['steering'].values

x_train, x_valid, y_train, y_valid = train_test_split(x, y, test_size=0.2, random_state=0)

model = load_model('model.h5')


x_test, y_test = test_set_image_prep('data/', x_valid, y_valid)


# test on test set
score = np.empty(x_test.shape[0])

for i in range(0,x_test.shape[0]):
    score[i] = model.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[i])

print('Test accuracy:', score[1])